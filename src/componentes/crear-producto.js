import React from "react";
import axios from "axios";
import { upload } from "@testing-library/user-event/dist/upload";

function CrearProducto() {
    function handleSubmit(e) {
        e.preventDefault();

        const formData = new FormData(e.target)
        const form = Object.fromEntries(formData.entries())

        console.log(form);
        
        let file = form.logo;
        let reader = new FileReader();
        reader.onload = function () {
            console.log(reader.result);
            const base64String = reader.result.replace("data:", "")
                .replace(/^.+,/, "");
                console.log(base64String);
                form.logo = base64String;

                axios.post('http://localhost:4000/producto', form).then(res => {
                    console.log(res);
                }).catch(err => {
                    console.error(err);
                })
        }
        reader.readAsDataURL(file);

    }
    return (
        <div className="crear-producto">
            <h1>Producto</h1>
            <form onSubmit={handleSubmit}>
                <div className="form-control">
                    <label>Nombre:</label> <input name="nombre"></input>
                </div>
                <div className="form-control">
                    <label>Valor:</label> <input name="valor"></input>
                </div>
                <div className="form-control">
                    <label>Categoria:</label> <input name="categoria"></input>
                </div>
                <div className="form-control">
                    <label>Vendedor:</label> <input name="vendedorId"></input>
                </div>
                <input type="file" name="logo"/>
                <button type="submit">Crear</button>
            </form>
        </div>
    );
}

export default CrearProducto;