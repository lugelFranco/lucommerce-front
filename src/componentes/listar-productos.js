import axios from "axios";
import { useEffect, useState } from "react";

function ListarProductos() {
    const [productos, setProductos] = useState([])
    const productosActualizados = 0;

    const cargarProductos = () => {
        axios.get('http://localhost:4000/producto').then(res => {
            console.log(res.data)
            setProductos(res.data)
        }).catch(err => {
            console.error(err);
        })
    }

    useEffect(() => {
        cargarProductos()
    }, [productosActualizados])

    function handleClick() {
        console.log('Buscando los productos')
        cargarProductos()
    }
    return (<div className="cargar-productos">
        <button onClick={handleClick}>Cargar productos</button>
        <ul>
            {
                productos.map(producto => <li>
                <img src={`data:image/jpeg;base64,${producto.logo}`} /> 
                <label>{producto.nombre} ${producto.valor}</label></li>)
            }

        </ul>
    </div>)
}

export default ListarProductos;