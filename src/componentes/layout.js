import { Outlet, Link } from "react-router-dom";

const Layout = () => {
    return (
        <>
            <nav>
                <ul>
                    <li>
                        <Link to="compra" >Compra </Link>
                    </li>
                    <li>
                        <Link to="productos" >Ver Productos </Link>
                    </li>
                    <li>
                        <Link to="crear-producto">Crear Producto</Link>
                    </li>
                </ul>
            </nav>

            <Outlet />
        </>
    )
};

export default Layout;