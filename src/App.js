import logo from './logo.svg';
import './App.css';
import CrearProducto from './componentes/crear-producto';
import ListarProductos from './componentes/listar-productos';
import VerCompraActual from './componentes/ver-compra-actual';

import { BrowserRouter,  Route, Routes, Link } from 'react-router-dom';
import Layout from './componentes/layout';

function App() {
  return (
    <div className="App">      
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Layout />}>
              <Route path="compra" element={<VerCompraActual />} />
              <Route path="productos" element={<ListarProductos />} />
              <Route path="crear-producto" element={<CrearProducto />} />
            </Route>
          </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
